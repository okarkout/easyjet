#!/bin/env python

import argparse
import datetime
import os
import subprocess
from shutil import which
import yaml
from EasyjetHub.steering.utils.config_files import run_config_arg


def main():
    parser = argparse.ArgumentParser()

    in_list = parser.add_mutually_exclusive_group(required=True)
    in_list.add_argument(
        "--mc-list",
        help="Text file containing MC datasets")
    in_list.add_argument(
        "--data-list",
        help="Text file containing data datasets"
    )

    parser.add_argument(
        "--run-config",
        type=str,
        required=True,
        help="file path to the runConfig",
    )
    parser.add_argument(
        "--exec",
        type=str,
        default="easyjet-ntupler",
        help="The name of the executable",
    )
    parser.add_argument(
        "--campaign",
        default="EJ_%Y_%m_%d",
        help=(
            "The name of the campaign. Will be prepended to the output dataset"
            " name. Can include datetime.strftime replacement fields"
        ),
    )
    parser.add_argument(
        "--asetup",
        help="The asetup command, in case it cannot be read from the environment",
    )
    parser.add_argument(
        "--dest-se",
        default=None,
        help="An RSE to duplicate the results to, for example 'DESY-ZN_LOCALGROUPDISK'",
    )
    parser.add_argument(
        "--excluded-site", default=None, help="Any sites to exclude when running"
    )
    parser.add_argument(
        "--nGBperJob", default=None, help="How many GB required per job"
    )
    parser.add_argument(
        "--nFiles", default=None, help="How many files to run on"
    )
    parser.add_argument(
        "--noSubmit",
        action="store_true",
        help="Don't submit the jobs, just create the tarball",
    )

    args = parser.parse_args()

    # check ntupler command is available before submitting
    if which(args.exec) is None:
        raise ValueError(f"Failed to find {args.exec}. Check that it is installed")

    # Prepare some of the prun options here
    submit_opts = {
        "mergeOutput": False,
        "outputs": "TREE:output-tree.root",
        "writeInputToTxt": "IN:in.txt",
        "useAthenaPackages": True,
    }

    if args.noSubmit:
        submit_opts["noSubmit"] = True
    if args.dest_se is not None:
        submit_opts["destSE"] = args.dest_se
    if args.excluded_site is not None:
        submit_opts["excludedSite"] = args.excluded_site
    if args.nGBperJob is not None:
        submit_opts["nGBPerJob"] = args.nGBperJob
    if args.nFiles is not None:
        submit_opts["nFiles"] = args.nFiles

    if args.asetup is None:
        atlas_project = os.environ["AtlasProject"]

        project_dir = os.environ["{0}_DIR".format(atlas_project)]
        project_version = os.environ["{0}_VERSION".format(atlas_project)]

        if project_dir.startswith("/cvmfs/atlas-nightlies.cern.ch"):
            raise ValueError(
                "Cannot deduce the asetup command for a nightly!"
                + " Use the --asetup option"
            )
        submit_opts["athenaTag"] = ",".join([atlas_project, project_version])
    else:
        submit_opts["athenaTag"] = args.asetup

    # check some stuff
    if not which("prun"):
        raise RuntimeError("you have not set up panda, run `lsetup panda!`")

    nickname = getGridNickname()

    # copy the runconfig to the submit dir to be available for the exec cmd
    try:
        with open('config.yaml','w') as cfg:
            cfg.write(yaml.dump(run_config_arg(args.run_config)))
    except subprocess.CalledProcessError as e:
        raise OSError(e.returncode, "Failed to copy run config files.")

    proc = subprocess.Popen(
        [
            "prun",
            "--outDS",
            "user.{0}.NONE".format(nickname),
            "--exec",
            "NONE",
            "--noSubmit",
            "--useAthenaPackages",
            "--outTarBall",
            "code.tar.gz",
        ]
    )

    if proc.wait() != 0:
        raise OSError(proc.returncode, "Failed to create tarball")

    submit_opts["inTarBall"] = "code.tar.gz"

    campaign = datetime.datetime.now().strftime(args.campaign)

    io_list = []
    if args.mc_list is not None:
        with open(args.mc_list) as fp:
            mc_list = fp.readlines()
        for line in mc_list:
            line = line.strip()
            if line.startswith("*"):
                continue
            if not line:
                continue
            ds_name = line.rpartition(":")[2]
            project, dsid, physics_short, prod_step, dtype, tags = ds_name.split(".")
            io_list.append(
                {
                    "inDS": ds_name,
                    "outDS": "user.{0}.{1}.{2}.{3}.{4}".format(
                        nickname, campaign, dsid, physics_short, tags
                    ),
                }
            )

    if args.data_list is not None:
        with open(args.data_list) as fp:
            data_list = fp.readlines()
        for line in data_list:
            line = line.strip()
            if line.startswith("*"):
                continue
            if not line:
                continue
            ds_name = line.rpartition(":")[2]
            split = ds_name.split(".")
            io_list.append(
                {
                    "inDS": ds_name,
                    "outDS": "user.{0}.{1}.{2}.{3}.{4}".format(
                        nickname, campaign, split[0], split[1], split[-1]
                    ),
                }
            )

    if not io_list:
        raise ValueError("No inputs defined")

    for io in io_list:
        exec_cmd = "{0} %IN -l --run-config {1} --out-file {2}".format(
            args.exec,
            "config.yaml",
            submit_opts["outputs"].removeprefix("TREE:"),
        )

        cmd = [
            "prun",
            "--inDS", io["inDS"],
            "--outDS", io["outDS"],
            "--exec", exec_cmd,
        ]

        for k, v in submit_opts.items():
            if isinstance(v, bool):
                if v:
                    cmd.append(f"--{k}")
            else:
                if isinstance(v, (list, tuple)):
                    v = ",".join(map(str, v))

                cmd += [f"--{k}", v]

        print(" ".join(cmd))
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = proc.communicate()
        print(out.decode("utf-8"), err.decode("utf-8"))
        if proc.returncode != 0:
            raise OSError(proc.returncode, err)


def getGridNickname():
    try:
        voms_proxy_info = subprocess.check_output(["voms-proxy-info", "--all"])
        voms_proxy_info = voms_proxy_info.decode("utf-8")
        nickname_index = voms_proxy_info.find("nickname")
        nickname_parts = voms_proxy_info[nickname_index:].split()
        nickname = nickname_parts[2]
        return nickname
    # this is slightly sloppy: we shoudl figure out what exception was
    # actually thrown before assuming it was a missing init.
    except Exception:
        raise OSError(
            1,
            "Could not find proxy information."
            + ' Try typing "voms-proxy-init -voms atlas" and try again',
        )


if __name__ == "__main__":
    main()
